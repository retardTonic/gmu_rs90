#!/bin/bash
set -e

mkdir -p opk
cp -rt opk/ decoders frontends default.rs90.desktop gmu-rs90.dge gmu.bin gmu.bmp gmu.png gmu.rs90.conf gmuinput.rs90.conf libs.rs90 rs90.keymap README.txt
mkdir -p opk/themes
cp -r themes/default-modern-rs90 opk/themes/default-modern
mksquashfs ./opk gmu_rs90.opk -all-root -noappend -no-exports -no-xattrs
rm -r opk
