#!/bin/bash
set -e

_dir="$(cd "$(dirname "$0")" && pwd -P)"
_host=mipsel-rs90-linux-musl
_toolchain="/opt/rs90-toolchain"
_sysroot="$_toolchain/mipsel-rs90-linux-musl/sysroot"
export PATH="$_toolchain/bin:$PATH"

_mpg123="$_dir/mpg123-1.25.13"

_copy() {
	echo "'$1'"
	if ! [ -a "$1" ]; then
		echo "$1 not found."
		exit 1
	fi
	cp --dereference "$1" "$_dir/libs.rs90"
}

_notice() {
	echo \
"-------------------------------------------------------------------------------

$1

-------------------------------------------------------------------------------"
}

mkdir -p "$_dir/libs.rs90"
rm -f "$_dir/libs.rs90"/*

_notice 'Building libmpg123'
cd "$_mpg123"
./configure \
	--host="$_host" \
	--enable-ipv6=no \
	--enable-modules=no \
	--enable-network=no \
	--with-audio=dummy \
	--with-cpu=generic_nofpu \
	--with-sysroot="$_sysroot"
make clean
make
_copy ./src/libmpg123/.libs/libmpg123.so.0

_notice 'Building libFLAC'
cd "$_dir/flac"
./autogen.sh
./configure \
	--host="$_host" \
	--enable-debug=no \
	--disable-cpplibs \
	--disable-doxygen-docs \
	--disable-examples \
	--disable-oggtest \
	--disable-xmms-plugin \
	--with-sysroot="$_sysroot"
make clean
make
_copy ./src/libFLAC/.libs/libFLAC.so.8

_notice 'Building libopusfile'
cd "$_dir/opusfile"
./autogen.sh
./configure \
	--host="$_host" \
	--disable-doc \
	--disable-examples \
	--enable-fixed-point \
	--disable-float \
	--disable-http \
	--with-sysroot="$_sysroot"
make clean
make
_copy ./.libs/libopusfile.so.0

_notice 'Building libmodplug'
cd "$_dir/libmodplug"
autoreconf -isf
./configure --host="$_host" --with-sysroot="$_sysroot"
make clean
make
_copy ./src/.libs/libmodplug.so.1

_notice 'Building gmu'
cd "$_dir"
CC=${_host}-gcc CFLAGS="-DSDLFE_NO_HWACCEL=1" LFLAGS="-Wl,-rpath-link,libs.rs90" ./configure \
	--sdk-path="$_sysroot/usr" \
	--target-device=rs90 \
	--enable=flac-decoder \
	--enable=modplug-decoder \
	--enable=mpg123-decoder \
	--disable=musepack-decoder \
	--disable=openmpt-decoder \
	--enable=opus-decoder \
	--disable=speex-decoder \
	--enable=vorbis-decoder \
	--disable=wavpack-decoder \
	--disable=lirc-frontend \
	--enable=sdl-frontend \
	--disable=web-frontend \
	--enable=SDL_gfx \
	--enable=debug \
	--includes="$_dir/src" \
	--includes="$_mpg123/src/libmpg123" \
	--libs="$_mpg123/src/libmpg123/.libs" \
	--includes="$_dir/flac/include" \
	--libs="$_dir/flac/src/libFLAC/.libs" \
	--includes="$_sysroot/usr/include/opus" \
	--includes="$_dir/opusfile/include" \
	--libs="$_dir/opusfile/.libs" \
	--includes="$_dir/libmodplug/src" \
	--libs="$_dir/libmodplug/src/.libs"
make clean
make

_notice 'Done!'
